A Rental House Searching Tool - Group 6

Introduction

Airbnb is an American company that operates an online marketplace for lodging, primarily homestays for vacation rentals, and tourism activities, and its web service provides rental house searching with conditions such as price and location. However, the conditions it provides are not comprehensive. For example, users are not able to search for rooms by filtering additional fees for extra guests, host response rate and cancellation policy, etc. For this project, we developed a high-efficient rental house searching tool with more comprehensive searching conditions available, helping users to find places more precisely within less than a second among half a million houses across over 20 countries. The tool provides three main functions, house searching, user favourites allowing users to add and review houses added to their favourites, and search history enabling users to review their search histories during a user-specified period. 

Main Functions

- Enable users to do the basic search operations with specified input conditions  
- Allow users to save their interesting search results by listing URL and retrieve whenever they want  
- Allow users to save their search history and re-apply those conditions  
- Write comments to the house that users put to their favourite list  
