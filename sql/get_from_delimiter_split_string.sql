CREATE FUNCTION split(
  in_array varchar(255),
  in_delimiter char(1),
  in_index int
)
RETURNS varchar(255)
RETURN REPLACE(
  SUBSTRING(
    SUBSTRING_INDEX(in_array, in_delimiter, in_index + 1),
    LENGTH(
      SUBSTRING_INDEX(in_array, in_delimiter, in_index)
    ) + 1
  ),
  in_delimiter,
  ''
);