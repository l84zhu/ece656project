-- host
INSERT INTO host 
(hostID, hostURL, hostName, hostSince, hostLocation, hostAbout, hostResponseTime, hostResponseRate, hostAcceptanceRate,
hostThumbnailUrl, hostPictureUrl, hostNeightbourhood, hostListingsCount, hostTotalListingsCount, calculatedHostListingsCount)
SELECT DISTINCT
hostID, hostURL, hostName, hostSince, hostLocation, hostAbout, hostResponseTime, hostResponseRate, hostAcceptanceRate,
hostThumbnailUrl, hostPictureUrl, hostNeightbourhood, hostListingsCount, hostTotalListingsCount, calculatedHostListingsCount
FROM airbnblistings;

-- locates
INSERT INTO locates
(SELECT DISTINCT countryID, stateID, cityID, streetID, hostID, ID, geolocation 
FROM street INNER JOIN airbnblistings on (streetName=street) INNER JOIN `host` using (hostID));

-- listings
INSERT INTO listings
SELECT DISTINCT hostID, ID, name, summary, space, description, notes, houseRules, 
experiencesOffered, access, transit, listingUrl, thumbnailUrl, mediumUrl, pictureUrl, XLPictureUrl
FROM airbnblistings;

-- calendar
INSERT INTO calendar(hostID, listingID, calendarUpdated, calendarLastScraped)
SELECT DISTINCT hostID, ID, calendarUpdated, calendarLastScraped
FROM airbnblistings;

-- amenity
INSERT INTO Amenity(hostID, listingID, propertyType, roomType, accommodates, bathrooms, bedrooms, bedType, bedNumber)
SELECT DISTINCT hostID, ID, propertyType, roomType, accommodates, bathrooms, bedrooms, bedType, beds
FROM airbnblistings;

-- reviews
INSERT INTO Reviews(hostID, listingID, numberOfReviews, firstReview, lastReview, reviewScoresRating, 
reviewScoresAccuracy, reviewScoresCleanliness, reviewScoresCheckin, reviewScoresCommunication, reviewScoresLocation, reviewScoresValue, reviewsPerMonth)
SELECT DISTINCT hostID, ID, numberOfReviews, firstReview, lastReview, reviewScoresRating, 
reviewScoresAccuracy, reviewScoresCleanliness, reviewScoresCheckin, reviewScoresCommunication, reviewScoresLocation, reviewScoresValue, reviewsPerMonth
FROM airbnblistings;

-- availability
INSERT INTO Availability(hostID, listingID, availability0, availability30, availability60, availability90, availability365)
SELECT DISTINCT hostID, ID, null, availability30, availability60, availability90, availability365
FROM airbnblistings;

-- price
INSERT INTO price(hostID, listingID, weeklyPrice, monthlyPrice, securityDeposit, cleaningFee, price, extraPeople)
SELECT DISTINCT hostID, ID,  weeklyPrice, monthlyPrice, securityDeposit, cleaningFee, price, extraPeople
FROM airbnblistings;

-- rules
INSERT INTO rules(hostID, listingID, minimumNights, maximumNights, guestsIncluded, cancellationPolicy)
SELECT DISTINCT hostID, ID, minimumNights, maximumNights, guestsIncluded, cancellationPolicy
FROM airbnblistings;

-- scrapeInfo
INSERT INTO scrapeinfo(hostID, listingID, lastScraped)
SELECT DISTINCT hostID, ID, lastScraped
FROM airbnblistings;

-- country
INSERT INTO country(country_name, country_code)
SELECT DISTINCT country, countryCode
FROM airbnblistings;

-- state
INSERT INTO state(countryID, state_name)
SELECT DISTINCT countryID, state
FROM airbnblistings inner join country where airbnblistings.country = country.country_name and airbnblistings.countryCode = country.country_code;

-- city
INSERT INTO city(countryID, stateID, city_name)
SELECT DISTINCT country.countryID, state.stateID, city
FROM airbnblistings inner join country on airbnblistings.country = country.country_name and airbnblistings.countryCode = country.country_code
inner join state on airbnblistings.state = state.state_name;

-- street
INSERT INTO street(countryID, stateID, cityID, streetName, zipCode)
SELECT DISTINCT country.countryID, state.stateID, city.cityID, street, zipcode
FROM airbnblistings inner join country on airbnblistings.country = country.country_name and airbnblistings.countryCode = country.country_code
inner join state on airbnblistings.state = state.state_name
inner join city on airbnblistings.city = city.city_name;

-- amenityInfo
DROP PROCEDURE IF EXISTS a;
CREATE PROCEDURE a()
BEGIN
	DECLARE j INT DEFAULT 6;
	DECLARE i INT DEFAULT 0;
	WHILE j <= 19291748 DO
		SET i = 0;
		WHILE (SELECT split(amenities,',',i) FROM airbnblistings WHERE ID = j) <> '' DO
			INSERT into amenityinfo (SELECT hostID, ID, split(amenities,',',i) FROM airbnblistings WHERE ID = j);
			SET i = i + 1;
		END WHILE;
		SET j = j + 1;
	END WHILE;
END;
call a;

-- features
DROP PROCEDURE IF EXISTS a;
CREATE PROCEDURE a()
BEGIN
	DECLARE j INT DEFAULT 6;
	DECLARE i INT DEFAULT 0;
	WHILE j <= 19291748 DO
		SET i = 0;
		WHILE (SELECT split(features,',',i) FROM airbnblistings WHERE ID = j) <> '' DO
			INSERT into features (SELECT DISTINCT hostID, ID, split(features,',',i) FROM airbnblistings WHERE ID = j);
			SET i = i + 1;
		END WHILE;
		SET j = j + 1;
	END WHILE;
END;
call a;

-- hostverification
DROP PROCEDURE IF EXISTS a;
CREATE PROCEDURE a()
BEGIN
	DECLARE j INT DEFAULT 0;
	DECLARE i INT DEFAULT 0;
	WHILE j <= 135088513 DO
		SET i = 0;
		WHILE (SELECT split(hostVerification,',',i) FROM temp WHERE hostID = j) <> '' DO
			INSERT into hostVerification (SELECT DISTINCT hostID, split(hostVerification,',',i) FROM temp WHERE hostID = j);
			SET i = i + 1;
		END WHILE;
		SET j = j + 1;
	END WHILE;
END;
call a;
