DROP TABLE if EXISTS features;
DROP TABLE if EXISTS scrapeinfo;
DROP TABLE if EXISTS locates;
DROP TABLE if EXISTS street;
DROP TABLE if EXISTS favorite;
DROP TABLE if EXISTS users;
DROP TABLE if EXISTS rules;
DROP TABLE if EXISTS street;
DROP TABLE if EXISTS city;
DROP TABLE if EXISTS state;
DROP TABLE if EXISTS country;
DROP TABLE if EXISTS calendar;
DROP TABLE if EXISTS Availability;
DROP TABLE if EXISTS UserReviews;
DROP TABLE if EXISTS AmenityInfo;
DROP TABLE IF EXISTS Amenity;
DROP TABLE if EXISTS Reviews;
DROP TABLE if EXISTS price;
DROP TABLE IF EXISTS hostverification;
DROP TABLE IF EXISTS listings;
DROP TABLE IF EXISTS host;

 CREATE TABLE `host` (
   `hostID` bigint NOT NULL,
   `hostURL` varchar(200) DEFAULT NULL,
   `hostName` char(20) DEFAULT NULL,
   `hostSince` date DEFAULT NULL,
   `hostLocation` char(150) DEFAULT NULL,
   `hostAbout` varchar(500) DEFAULT NULL,
   `hostResponseTime` char(20) DEFAULT NULL,
   `hostResponseRate` int DEFAULT NULL,
   `hostAcceptanceRate` int DEFAULT NULL,
   `hostThumbnailUrl` char(200) DEFAULT NULL,
   `hostPictureUrl` char(200) DEFAULT NULL,
   `hostNeightbourhood` char(30) DEFAULT NULL,
   `hostListingsCount` int DEFAULT NULL,
   `hostTotalListingsCount` int DEFAULT NULL,
   `calculatedHostListingsCount` int DEFAULT NULL,
   PRIMARY KEY (`hostID`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `hostverification` (
   `hostID` bigint NOT NULL,
   `hostVerification` char(30) NOT NULL,
   PRIMARY KEY (`hostID`,`hostVerification`),
   CONSTRAINT `hostverification_ibfk_1` FOREIGN KEY (`hostID`) REFERENCES `host` (`hostID`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

 CREATE TABLE `listings` (
   `hostID` bigint NOT NULL,
   `listingID` int NOT NULL,
   `summary` varchar(1000) DEFAULT NULL,
   `description_space` varchar(1000) DEFAULT NULL,
   `description_other` varchar(1000) DEFAULT NULL,
   `notes` varchar(1000) DEFAULT NULL,
   `house_rules` varchar(800) DEFAULT NULL,
   `experiences_offered` varchar(100) DEFAULT NULL,
   `access_info` varchar(1000) DEFAULT NULL,
   `access_transit` varchar(1000) DEFAULT NULL,
   `url_listing` varchar(200) DEFAULT NULL,
   `url_thumbnail` varchar(200) DEFAULT NULL,
   `url_medium` varchar(200) DEFAULT NULL,
   `url_pic` varchar(200) DEFAULT NULL,
   `url_xl_pic` varchar(200) DEFAULT NULL,
   `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
   PRIMARY KEY (`hostID`,`listingID`),
   KEY `listingID` (`listingID`),
   CONSTRAINT `listings_ibfk_1` FOREIGN KEY (`hostID`) REFERENCES `host` (`hostID`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `price` (
   `priceID` int NOT NULL AUTO_INCREMENT,
   `hostID` bigint NOT NULL,
   `listingID` int NOT NULL,
   `weekly_price` int DEFAULT NULL,
   `monthly_price` int DEFAULT NULL,
   `security_deposit` int DEFAULT NULL,
   `cleaning_fee` int DEFAULT NULL,
   `price` decimal(10,2) DEFAULT NULL,
   `extra_people` int DEFAULT NULL,
   PRIMARY KEY (`priceID`,`hostID`,`listingID`),
   KEY `hostID` (`hostID`,`listingID`),
   CONSTRAINT `price_ibfk_1` FOREIGN KEY (`hostID`, `listingID`) REFERENCES `listings` (`hostID`, `listingID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=487984 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `calendar` (
   `calendarID` int NOT NULL AUTO_INCREMENT,
   `hostID` bigint NOT NULL,
   `listingID` int NOT NULL,
   `calendarUpdated` char(15) DEFAULT NULL,
   `calendarLastScraped` date DEFAULT NULL,
   PRIMARY KEY (`calendarID`,`hostID`,`listingID`),
   KEY `hostID` (`hostID`,`listingID`),
   CONSTRAINT `calendar_ibfk_1` FOREIGN KEY (`hostID`, `listingID`) REFERENCES `listings` (`hostID`, `listingID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=487984 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `reviews` (
   `reviewID` int NOT NULL AUTO_INCREMENT,
   `hostID` bigint NOT NULL,
   `listingID` int NOT NULL,
   `numberOfReviews` int DEFAULT NULL,
   `firstReview` date DEFAULT NULL,
   `lastReview` date DEFAULT NULL,
   `reviewScoresRating` int DEFAULT NULL,
   `reviewScoresAccuracy` int DEFAULT NULL,
   `reviewScoresCleanliness` int DEFAULT NULL,
   `reviewScoresCheckin` int DEFAULT NULL,
   `reviewScoresCommunication` int DEFAULT NULL,
   `reviewScoresLocation` int DEFAULT NULL,
   `reviewScoresValue` int DEFAULT NULL,
   `reviewsPerMonth` decimal(5,2) DEFAULT NULL,
   PRIMARY KEY (`reviewID`,`hostID`,`listingID`),
   KEY `reviews_ibfk_1` (`hostID`,`listingID`),
   CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`hostID`, `listingID`) REFERENCES `listings` (`hostID`, `listingID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=487984 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `amenity` (
   `amenityID` int NOT NULL AUTO_INCREMENT,
   `hostID` bigint NOT NULL,
   `listingID` int NOT NULL,
   `propertyType` char(20) DEFAULT NULL,
   `roomType` char(20) DEFAULT NULL,
   `accommodates` int DEFAULT NULL,
   `bathrooms` int DEFAULT NULL,
   `bedrooms` int DEFAULT NULL,
   `bedType` char(30) DEFAULT NULL,
   `bedNumber` int DEFAULT NULL,
   PRIMARY KEY (`amenityID`,`hostID`,`listingID`),
   KEY `amenity_ibfk_1` (`hostID`,`listingID`),
   CONSTRAINT `amenity_ibfk_1` FOREIGN KEY (`hostID`, `listingID`) REFERENCES `listings` (`hostID`, `listingID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=487984 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `amenityinfo` (
   `hostID` bigint NOT NULL,
   `listingID` int NOT NULL,
   `amenities` varchar(50) NOT NULL,
   `amenityID` int NOT NULL,
   PRIMARY KEY (`hostID`,`listingID`,`amenities`,`amenityID`) USING BTREE,
   KEY `amenityinfo_ibfk_1` (`hostID`,`listingID`,`amenityID`),
   CONSTRAINT `amenityinfo_ibfk_1` FOREIGN KEY (`hostID`, `listingID`, `amenityID`) REFERENCES `amenity` (`hostID`, `listingID`, `amenityID`) ON DELETE RESTRICT ON UPDATE RESTRICT
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `availability` (
   `availabilityID` int NOT NULL AUTO_INCREMENT,
   `hostID` bigint NOT NULL,
   `listingID` int NOT NULL,
   `availability0` int DEFAULT NULL,
   `availability30` int NOT NULL,
   `availability60` int NOT NULL,
   `availability90` int NOT NULL,
   `availability365` int NOT NULL,
   PRIMARY KEY (`availabilityID`,`hostID`,`listingID`),
   KEY `hostID` (`hostID`,`listingID`),
   CONSTRAINT `availability_ibfk_1` FOREIGN KEY (`hostID`, `listingID`) REFERENCES `listings` (`hostID`, `listingID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=487984 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

 CREATE TABLE `country` (
   `countryID` int NOT NULL AUTO_INCREMENT,
   `country_name` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
   `country_code` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
   PRIMARY KEY (`countryID`),
   KEY `countryName` (`country_name`)
 ) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

 CREATE TABLE `state` (
   `countryID` int NOT NULL,
   `stateID` int NOT NULL AUTO_INCREMENT,
   `state_name` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
   PRIMARY KEY (`countryID`,`stateID`),
   KEY `stateName` (`state_name`),
   KEY `stateID` (`stateID`),
   CONSTRAINT `state_ibfk_1` FOREIGN KEY (`countryID`) REFERENCES `country` (`countryID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=2048 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

 CREATE TABLE `city` (
   `countryID` int NOT NULL,
   `stateID` int NOT NULL,
   `cityID` int NOT NULL AUTO_INCREMENT,
   `city_name` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
   PRIMARY KEY (`countryID`,`stateID`,`cityID`),
   KEY `cityName` (`city_name`),
   KEY `cityID` (`cityID`),
   CONSTRAINT `city_ibfk_1` FOREIGN KEY (`countryID`, `stateID`) REFERENCES `state` (`countryID`, `stateID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=16384 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

 CREATE TABLE `street` (
   `countryID` int NOT NULL,
   `stateID` int NOT NULL,
   `cityID` int NOT NULL,
   `streetID` int NOT NULL AUTO_INCREMENT,
   `streetName` char(200) NOT NULL,
   `zipCode` char(15) DEFAULT NULL,
   PRIMARY KEY (`countryID`,`stateID`,`cityID`,`streetID`),
   KEY `streetID` (`streetID`),
   CONSTRAINT `street_ibfk_1` FOREIGN KEY (`countryID`, `stateID`, `cityID`) REFERENCES `city` (`countryID`, `stateID`, `cityID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=131071 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `locates` (
   `countryID` int NOT NULL,
   `stateID` int NOT NULL,
   `cityID` int NOT NULL,
   `streetID` int NOT NULL,
   `hostID` bigint NOT NULL,
   `listingID` int NOT NULL,
   `geolocation` char(40) DEFAULT NULL,
   PRIMARY KEY (`countryID`,`stateID`,`cityID`,`streetID`,`hostID`,`listingID`),
   KEY `hostID` (`hostID`,`listingID`),
   CONSTRAINT `locates_ibfk_1` FOREIGN KEY (`countryID`, `stateID`, `cityID`, `streetID`) REFERENCES `street` (`countryID`, `stateID`, `cityID`, `streetID`),
   CONSTRAINT `locates_ibfk_2` FOREIGN KEY (`hostID`, `listingID`) REFERENCES `listings` (`hostID`, `listingID`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `scrapeinfo` (
   `hostID` bigint NOT NULL,
   `listingID` int NOT NULL,
   `scrapeID` bigint NOT NULL AUTO_INCREMENT,
   `lastScraped` date DEFAULT NULL,
   PRIMARY KEY (`hostID`,`listingID`,`scrapeID`),
   KEY `scrapeID` (`scrapeID`),
   CONSTRAINT `scrapeinfo_ibfk_1` FOREIGN KEY (`hostID`, `listingID`) REFERENCES `listings` (`hostID`, `listingID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=487984 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `users` (
   `userID` int NOT NULL AUTO_INCREMENT,
   `userName` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
   `userPhone` bigint DEFAULT NULL,
   `userEmail` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
   `userCountry` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
   `userState` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
   `userCity` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
   `userAddress` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`userID`),
   KEY `userCountry` (`userCountry`),
   KEY `userState` (`userState`),
   KEY `userCity` (`userCity`),
   CONSTRAINT `users_ibfk_1` FOREIGN KEY (`userCountry`) REFERENCES `country` (`country_name`),
   CONSTRAINT `users_ibfk_2` FOREIGN KEY (`userState`) REFERENCES `state` (`state_name`),
   CONSTRAINT `users_ibfk_3` FOREIGN KEY (`userCity`) REFERENCES `city` (`city_name`)
 ) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `favorite` (
   `userID` int NOT NULL,
   `hostID` bigint NOT NULL,
   `listingID` int NOT NULL,
   `memo` varchar(500) DEFAULT NULL,
   PRIMARY KEY (`userID`,`hostID`,`listingID`),
   KEY `hostID` (`hostID`,`listingID`),
   CONSTRAINT `favorite_ibfk_1` FOREIGN KEY (`hostID`, `listingID`) REFERENCES `listings` (`hostID`, `listingID`),
   CONSTRAINT `favorite_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rules` (
   `hostID` bigint NOT NULL,
   `listingID` int NOT NULL,
   `ruleID` int NOT NULL AUTO_INCREMENT,
   `minimumNights` int DEFAULT NULL,
   `maximumNights` int DEFAULT NULL,
   `guestsIncluded` int DEFAULT NULL,
   `cancellationPolicy` varchar(50) DEFAULT NULL,
   PRIMARY KEY (`hostID`,`listingID`,`ruleID`),
   KEY `ruleID` (`ruleID`),
   CONSTRAINT `rules_ibfk_1` FOREIGN KEY (`hostID`, `listingID`) REFERENCES `listings` (`hostID`, `listingID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=487984 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `features` (
   `hostID` bigint NOT NULL,
   `listingID` int NOT NULL,
   `feature` varchar(50) NOT NULL,
   PRIMARY KEY (`hostID`,`listingID`,`feature`),
   CONSTRAINT `features_ibfk_1` FOREIGN KEY (`hostID`, `listingID`) REFERENCES `listings` (`hostID`, `listingID`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `searchhistory` (
   `userID` int NOT NULL,
   `historyID` int NOT NULL AUTO_INCREMENT,
   `searchCondition` varchar(1000) DEFAULT NULL,
   `searchDateTime` datetime DEFAULT NULL,
   PRIMARY KEY (`historyID`,`userID`),
   KEY `userID` (`userID`),
   CONSTRAINT `searchhistory_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`)
 ) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;










