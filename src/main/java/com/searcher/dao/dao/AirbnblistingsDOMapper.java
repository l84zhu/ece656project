package com.searcher.dao.dao;

import com.searcher.dao.model.AirbnblistingsDO;

import java.util.List;

public interface AirbnblistingsDOMapper {

    void recordHistory(AirbnblistingsDO record);

    List<AirbnblistingsDO> selectFavoByUser(Integer userid);

    List<AirbnblistingsDO> selectListingsByCond(AirbnblistingsDO record);

    List<AirbnblistingsDO> selectHistrByUserDay(AirbnblistingsDO record);

    List<AirbnblistingsDO> checkFavo(AirbnblistingsDO record);

    List<AirbnblistingsDO> checkUser(AirbnblistingsDO record);

    List<AirbnblistingsDO> getUserByName(String name);

    int add(AirbnblistingsDO record);

    int addUser(AirbnblistingsDO record);

    int deleteFavoByUserNameUrl (AirbnblistingsDO record);

    int deleteHistory (AirbnblistingsDO record);
}