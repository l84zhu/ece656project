package com.searcher.dao.model;

import java.math.BigDecimal;
import java.util.Date;

public class AirbnblistingsDO {
    private String searchCondition;

    private Date searchDateTime;

    private Date searchDateTimeEnd;

    private Integer historyID;

    private Integer listingID;

    private Integer hostID;

    private String summary;

    private String descriptionSpace;

    private String descriptionOther;

    private String notes;

    private String houseRules;

    private String experiencesOffered;

    private String accessInfo;

    private String accessTransit;

    private String urlListing;

    private String urlThumbnail;

    private String urlMedium;

    private String urlPic;

    private String urlXlPic;

    private String name;

    private String propertytype;

    private String roomtype;

    private Integer accommodates;

    private Integer bathrooms;

    private Integer bedrooms;

    private String bedtype;

    private Integer bednumber;

    private String amenities;

    private Integer availability0;

    private Integer availability30;

    private Integer availability60;

    private Integer availability90;

    private Integer availability365;

    private String calendarupdated;

    private Date calendarlastscraped;

    private String cityName;

    private String countryName;

    private String countryCode;

    private Integer userid;

    private String memo;

    private String feature;

    private String hosturl;

    private String hostname;

    private Date hostsince;

    private String hostlocation;

    private String hostabout;

    private String hostresponsetime;

    private Integer hostresponserate;

    private Integer hostacceptancerate;

    private String hostthumbnailurl;

    private String hostpictureurl;

    private String hostneightbourhood;

    private Integer hostlistingscount;

    private Integer hosttotallistingscount;

    private Integer calculatedhostlistingscount;

    private String hostverification;

    private String geolocation;

    private Integer weeklyPrice;

    private Integer monthlyPrice;

    private Integer securityDeposit;

    private Integer cleaningFee;

    private BigDecimal price;

    private Integer extraPeople;

    private Integer numberofreviews;

    private Date firstreview;

    private Date lastreview;

    private Integer reviewscoresrating;

    private Integer reviewscoresaccuracy;

    private Integer reviewscorescleanliness;

    private Integer reviewscorescheckin;

    private Integer reviewscorescommunication;

    private Integer reviewscoreslocation;

    private Integer reviewscoresvalue;

    private BigDecimal reviewspermonth;

    private Integer minimumnights;

    private Integer maximumnights;

    private Integer guestsincluded;

    private String cancellationpolicy;

    private Date lastscraped;

    private String stateName;

    private String streetName;

    private String zipcode;

    private String username;

    private Long userphone;

    private String useremail;

    private String usercountry;

    private String userstate;

    private String usercity;

    private String useraddress;

    public Integer getListingID() {
        return listingID;
    }

    public void setListingID(Integer listingID) {
        this.listingID = listingID;
    }

    public Integer getHostID() {
        return hostID;
    }

    public void setHostID(Integer hostID) {
        this.hostID = hostID;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary == null ? null : summary.trim();
    }

    public String getDescriptionSpace() {
        return descriptionSpace;
    }

    public void setDescriptionSpace(String descriptionSpace) {
        this.descriptionSpace = descriptionSpace == null ? null : descriptionSpace.trim();
    }

    public String getDescriptionOther() {
        return descriptionOther;
    }

    public void setDescriptionOther(String descriptionOther) {
        this.descriptionOther = descriptionOther == null ? null : descriptionOther.trim();
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes == null ? null : notes.trim();
    }

    public String getHouseRules() {
        return houseRules;
    }

    public void setHouseRules(String houseRules) {
        this.houseRules = houseRules == null ? null : houseRules.trim();
    }

    public String getExperiencesOffered() {
        return experiencesOffered;
    }

    public void setExperiencesOffered(String experiencesOffered) {
        this.experiencesOffered = experiencesOffered == null ? null : experiencesOffered.trim();
    }

    public String getAccessInfo() {
        return accessInfo;
    }

    public void setAccessInfo(String accessInfo) {
        this.accessInfo = accessInfo == null ? null : accessInfo.trim();
    }

    public String getAccessTransit() {
        return accessTransit;
    }

    public void setAccessTransit(String accessTransit) {
        this.accessTransit = accessTransit == null ? null : accessTransit.trim();
    }

    public String getUrlListing() {
        return urlListing;
    }

    public void setUrlListing(String urlListing) {
        this.urlListing = urlListing == null ? null : urlListing.trim();
    }

    public String getUrlThumbnail() {
        return urlThumbnail;
    }

    public void setUrlThumbnail(String urlThumbnail) {
        this.urlThumbnail = urlThumbnail == null ? null : urlThumbnail.trim();
    }

    public String getUrlMedium() {
        return urlMedium;
    }

    public void setUrlMedium(String urlMedium) {
        this.urlMedium = urlMedium == null ? null : urlMedium.trim();
    }

    public String getUrlPic() {
        return urlPic;
    }

    public void setUrlPic(String urlPic) {
        this.urlPic = urlPic == null ? null : urlPic.trim();
    }

    public String getUrlXlPic() {
        return urlXlPic;
    }

    public void setUrlXlPic(String urlXlPic) {
        this.urlXlPic = urlXlPic == null ? null : urlXlPic.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPropertytype() {
        return propertytype;
    }

    public void setPropertytype(String propertytype) {
        this.propertytype = propertytype == null ? null : propertytype.trim();
    }

    public String getRoomtype() {
        return roomtype;
    }

    public void setRoomtype(String roomtype) {
        this.roomtype = roomtype == null ? null : roomtype.trim();
    }

    public Integer getAccommodates() {
        return accommodates;
    }

    public void setAccommodates(Integer accommodates) {
        this.accommodates = accommodates;
    }

    public Integer getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(Integer bathrooms) {
        this.bathrooms = bathrooms;
    }

    public Integer getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(Integer bedrooms) {
        this.bedrooms = bedrooms;
    }

    public String getBedtype() {
        return bedtype;
    }

    public void setBedtype(String bedtype) {
        this.bedtype = bedtype == null ? null : bedtype.trim();
    }

    public Integer getBednumber() {
        return bednumber;
    }

    public void setBednumber(Integer bednumber) {
        this.bednumber = bednumber;
    }

    public String getAmenities() {
        return amenities;
    }

    public void setAmenities(String amenities) {
        this.amenities = amenities == null ? null : amenities.trim();
    }

    public Integer getAvailability0() {
        return availability0;
    }

    public void setAvailability0(Integer availability0) {
        this.availability0 = availability0;
    }

    public Integer getAvailability30() {
        return availability30;
    }

    public void setAvailability30(Integer availability30) {
        this.availability30 = availability30;
    }

    public Integer getAvailability60() {
        return availability60;
    }

    public void setAvailability60(Integer availability60) {
        this.availability60 = availability60;
    }

    public Integer getAvailability90() {
        return availability90;
    }

    public void setAvailability90(Integer availability90) {
        this.availability90 = availability90;
    }

    public Integer getAvailability365() {
        return availability365;
    }

    public void setAvailability365(Integer availability365) {
        this.availability365 = availability365;
    }

    public String getCalendarupdated() {
        return calendarupdated;
    }

    public void setCalendarupdated(String calendarupdated) {
        this.calendarupdated = calendarupdated == null ? null : calendarupdated.trim();
    }

    public Date getCalendarlastscraped() {
        return calendarlastscraped;
    }

    public void setCalendarlastscraped(Date calendarlastscraped) {
        this.calendarlastscraped = calendarlastscraped;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName == null ? null : cityName.trim();
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName == null ? null : countryName.trim();
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode == null ? null : countryCode.trim();
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature == null ? null : feature.trim();
    }

    public String getHosturl() {
        return hosturl;
    }

    public void setHosturl(String hosturl) {
        this.hosturl = hosturl == null ? null : hosturl.trim();
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname == null ? null : hostname.trim();
    }

    public Date getHostsince() {
        return hostsince;
    }

    public void setHostsince(Date hostsince) {
        this.hostsince = hostsince;
    }

    public String getHostlocation() {
        return hostlocation;
    }

    public void setHostlocation(String hostlocation) {
        this.hostlocation = hostlocation == null ? null : hostlocation.trim();
    }

    public String getHostabout() {
        return hostabout;
    }

    public void setHostabout(String hostabout) {
        this.hostabout = hostabout == null ? null : hostabout.trim();
    }

    public String getHostresponsetime() {
        return hostresponsetime;
    }

    public void setHostresponsetime(String hostresponsetime) {
        this.hostresponsetime = hostresponsetime == null ? null : hostresponsetime.trim();
    }

    public Integer getHostresponserate() {
        return hostresponserate;
    }

    public void setHostresponserate(Integer hostresponserate) {
        this.hostresponserate = hostresponserate;
    }

    public Integer getHostacceptancerate() {
        return hostacceptancerate;
    }

    public void setHostacceptancerate(Integer hostacceptancerate) {
        this.hostacceptancerate = hostacceptancerate;
    }

    public String getHostthumbnailurl() {
        return hostthumbnailurl;
    }

    public void setHostthumbnailurl(String hostthumbnailurl) {
        this.hostthumbnailurl = hostthumbnailurl == null ? null : hostthumbnailurl.trim();
    }

    public String getHostpictureurl() {
        return hostpictureurl;
    }

    public void setHostpictureurl(String hostpictureurl) {
        this.hostpictureurl = hostpictureurl == null ? null : hostpictureurl.trim();
    }

    public String getHostneightbourhood() {
        return hostneightbourhood;
    }

    public void setHostneightbourhood(String hostneightbourhood) {
        this.hostneightbourhood = hostneightbourhood == null ? null : hostneightbourhood.trim();
    }

    public Integer getHostlistingscount() {
        return hostlistingscount;
    }

    public void setHostlistingscount(Integer hostlistingscount) {
        this.hostlistingscount = hostlistingscount;
    }

    public Integer getHosttotallistingscount() {
        return hosttotallistingscount;
    }

    public void setHosttotallistingscount(Integer hosttotallistingscount) {
        this.hosttotallistingscount = hosttotallistingscount;
    }

    public Integer getCalculatedhostlistingscount() {
        return calculatedhostlistingscount;
    }

    public void setCalculatedhostlistingscount(Integer calculatedhostlistingscount) {
        this.calculatedhostlistingscount = calculatedhostlistingscount;
    }

    public String getHostverification() {
        return hostverification;
    }

    public void setHostverification(String hostverification) {
        this.hostverification = hostverification == null ? null : hostverification.trim();
    }

    public String getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(String geolocation) {
        this.geolocation = geolocation == null ? null : geolocation.trim();
    }

    public Integer getWeeklyPrice() {
        return weeklyPrice;
    }

    public void setWeeklyPrice(Integer weeklyPrice) {
        this.weeklyPrice = weeklyPrice;
    }

    public Integer getMonthlyPrice() {
        return monthlyPrice;
    }

    public void setMonthlyPrice(Integer monthlyPrice) {
        this.monthlyPrice = monthlyPrice;
    }

    public Integer getSecurityDeposit() {
        return securityDeposit;
    }

    public void setSecurityDeposit(Integer securityDeposit) {
        this.securityDeposit = securityDeposit;
    }

    public Integer getCleaningFee() {
        return cleaningFee;
    }

    public void setCleaningFee(Integer cleaningFee) {
        this.cleaningFee = cleaningFee;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getExtraPeople() {
        return extraPeople;
    }

    public void setExtraPeople(Integer extraPeople) {
        this.extraPeople = extraPeople;
    }

    public Integer getNumberofreviews() {
        return numberofreviews;
    }

    public void setNumberofreviews(Integer numberofreviews) {
        this.numberofreviews = numberofreviews;
    }

    public Date getFirstreview() {
        return firstreview;
    }

    public void setFirstreview(Date firstreview) {
        this.firstreview = firstreview;
    }

    public Date getLastreview() {
        return lastreview;
    }

    public void setLastreview(Date lastreview) {
        this.lastreview = lastreview;
    }

    public Integer getReviewscoresrating() {
        return reviewscoresrating;
    }

    public void setReviewscoresrating(Integer reviewscoresrating) {
        this.reviewscoresrating = reviewscoresrating;
    }

    public Integer getReviewscoresaccuracy() {
        return reviewscoresaccuracy;
    }

    public void setReviewscoresaccuracy(Integer reviewscoresaccuracy) {
        this.reviewscoresaccuracy = reviewscoresaccuracy;
    }

    public Integer getReviewscorescleanliness() {
        return reviewscorescleanliness;
    }

    public void setReviewscorescleanliness(Integer reviewscorescleanliness) {
        this.reviewscorescleanliness = reviewscorescleanliness;
    }

    public Integer getReviewscorescheckin() {
        return reviewscorescheckin;
    }

    public void setReviewscorescheckin(Integer reviewscorescheckin) {
        this.reviewscorescheckin = reviewscorescheckin;
    }

    public Integer getReviewscorescommunication() {
        return reviewscorescommunication;
    }

    public void setReviewscorescommunication(Integer reviewscorescommunication) {
        this.reviewscorescommunication = reviewscorescommunication;
    }

    public Integer getReviewscoreslocation() {
        return reviewscoreslocation;
    }

    public void setReviewscoreslocation(Integer reviewscoreslocation) {
        this.reviewscoreslocation = reviewscoreslocation;
    }

    public Integer getReviewscoresvalue() {
        return reviewscoresvalue;
    }

    public void setReviewscoresvalue(Integer reviewscoresvalue) {
        this.reviewscoresvalue = reviewscoresvalue;
    }

    public BigDecimal getReviewspermonth() {
        return reviewspermonth;
    }

    public void setReviewspermonth(BigDecimal reviewspermonth) {
        this.reviewspermonth = reviewspermonth;
    }

    public Integer getMinimumnights() {
        return minimumnights;
    }

    public void setMinimumnights(Integer minimumnights) {
        this.minimumnights = minimumnights;
    }

    public Integer getMaximumnights() {
        return maximumnights;
    }

    public void setMaximumnights(Integer maximumnights) {
        this.maximumnights = maximumnights;
    }

    public Integer getGuestsincluded() {
        return guestsincluded;
    }

    public void setGuestsincluded(Integer guestsincluded) {
        this.guestsincluded = guestsincluded;
    }

    public String getCancellationpolicy() {
        return cancellationpolicy;
    }

    public void setCancellationpolicy(String cancellationpolicy) {
        this.cancellationpolicy = cancellationpolicy == null ? null : cancellationpolicy.trim();
    }

    public Date getLastscraped() {
        return lastscraped;
    }

    public void setLastscraped(Date lastscraped) {
        this.lastscraped = lastscraped;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName == null ? null : stateName.trim();
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName == null ? null : streetName.trim();
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode == null ? null : zipcode.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public Long getUserphone() {
        return userphone;
    }

    public void setUserphone(Long userphone) {
        this.userphone = userphone;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail == null ? null : useremail.trim();
    }

    public String getUsercountry() {
        return usercountry;
    }

    public void setUsercountry(String usercountry) {
        this.usercountry = usercountry == null ? null : usercountry.trim();
    }

    public String getUserstate() {
        return userstate;
    }

    public void setUserstate(String userstate) {
        this.userstate = userstate == null ? null : userstate.trim();
    }

    public String getUsercity() {
        return usercity;
    }

    public void setUsercity(String usercity) {
        this.usercity = usercity == null ? null : usercity.trim();
    }

    public String getUseraddress() {
        return useraddress;
    }

    public void setUseraddress(String useraddress) {
        this.useraddress = useraddress == null ? null : useraddress.trim();
    }

    public String getSearchCondition() {
        return searchCondition;
    }

    public void setSearchCondition(String searchCondition) {
        this.searchCondition = searchCondition == null ? null : searchCondition.trim();
    }

    public Date getSearchDateTime() {
        return searchDateTime;
    }

    public void setSearchDateTime(Date searchDateTime) {
        this.searchDateTime = searchDateTime;
    }

    public Date getSearchDateTimeEnd() {
        return searchDateTimeEnd;
    }

    public void setSearchDateTimeEnd(Date searchDateTime) {
        this.searchDateTimeEnd = searchDateTime;
    }

    public Integer getHistoryID() {
        return historyID;
    }

    public void setHistoryID(Integer historyID) {
        this.historyID = historyID;
    }
}