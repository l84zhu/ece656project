package com.searcher.rest;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.searcher.rest.model.*;
import com.searcher.service.AirbnblistingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/")
public class AirbnblistingsRest {

    private final static Logger logger = LoggerFactory.getLogger(AirbnblistingsRest.class);

    @Autowired
    private AirbnblistingsService airbnblistingsService;

    @RequestMapping("/")
    public String index(){
        return "index.html";
    }
    
    @PostMapping("/queryListingsByConditionSelective")
    @ResponseBody
    public Object queryListingsByConditionSelective(@RequestBody AirbnblistingsVO airbnblistings) throws IOException {
        logger.info("queryListingsByCond{}" + JSONObject.toJSONString(airbnblistings));
        HashMap<String, Object> map = new HashMap<>();
        StopWatch watch = new StopWatch();
        watch.start();
        BaseResp userResp = airbnblistingsService.queryListingsByConditionSelective(airbnblistings);
        watch.stop();
        long totalTime = watch.getTotalTimeMillis();
        map.put("duration", totalTime);
        ObjectMapper mapper = new ObjectMapper();
        List<ResObjListing> list;
        if(userResp.getResJson() != null){
            list = Arrays.asList(mapper.readValue(userResp.getResJson(), ResObjListing[].class));
        }else{
            list = null;
        }
        map.put("result", list);
        map.put("message", userResp.getMsg());
        return map;
    }

    @PostMapping(value = "/addToFavorites")
    @ResponseBody
    public Object addToFavorites(@RequestBody AirbnblistingsVO airbnblistings) {
        logger.info("addToFavorites{}" + JSONObject.toJSONString(airbnblistings));
        HashMap<String, Object> map = new HashMap<>();
        StopWatch watch = new StopWatch();
        watch.start();
        BaseResp<AirbnblistingsVO> userResp = airbnblistingsService.addToFavorites(airbnblistings);
        watch.stop();
        long totalTime = watch.getTotalTimeMillis();
        map.put("duration", totalTime);
        map.put("result", new ArrayList<>());
        map.put("message", userResp.getMsg());
        return map;

    }

    @PostMapping("/getFavoritesByUser")
    @ResponseBody
    public Object getFavoritesByUser(@RequestBody AirbnblistingsVO airbnblistings) throws IOException {
        logger.info("getFavoritesByUser{}" + JSONObject.toJSONString(airbnblistings));
        HashMap<String, Object> map = new HashMap<>();
        StopWatch watch = new StopWatch();
        watch.start();
        BaseResp userResp = airbnblistingsService.getFavoritesByUser(airbnblistings);
        watch.stop();
        long totalTime = watch.getTotalTimeMillis();
        map.put("duration", totalTime);
        ObjectMapper mapper = new ObjectMapper();
        List<ResObjFav> list;
        if(userResp.getResJson() != null){
            list = Arrays.asList(mapper.readValue(userResp.getResJson(), ResObjFav[].class));
        }else{
            list = null;
        }
        map.put("result", list);
        map.put("message", userResp.getMsg());
        return map;
    }

    @PostMapping("/getHistoryByUserDay")
    @ResponseBody
    public Object getHistoryByUserDay(@RequestBody AirbnblistingsVO airbnblistings) throws IOException {
        logger.info("getHistoryByUserDay{}" + JSONObject.toJSONString(airbnblistings));
        HashMap<String, Object> map = new HashMap<>();
        StopWatch watch = new StopWatch();
        watch.start();
        BaseResp<AirbnblistingsVO> userResp = airbnblistingsService.getHistoryByUserDay(airbnblistings);
        watch.stop();
        long totalTime = watch.getTotalTimeMillis();
        map.put("duration", totalTime);

        ObjectMapper mapper = new ObjectMapper();
        List<ResObjHistory> list;
        if(userResp.getResJson() != null){
            list = Arrays.asList(mapper.readValue(userResp.getResJson(), ResObjHistory[].class));
        }else{
            list = null;
        }
        map.put("result", list);
        map.put("message", userResp.getMsg());

        return map;
    }

    @PostMapping("/registerUser")
    @ResponseBody
    public Object registerUser(@RequestBody AirbnblistingsVO airbnblistings){
        logger.info("registerUser{}" + JSONObject.toJSONString(airbnblistings));
        HashMap<String, Object> map = new HashMap<>();
        StopWatch watch = new StopWatch();
        watch.start();
        BaseResp<AirbnblistingsVO> userResp = airbnblistingsService.registerUser(airbnblistings);
        watch.stop();
        long totalTime = watch.getTotalTimeMillis();
        map.put("duration", totalTime);
        map.put("result", new ArrayList<>());
        map.put("message", userResp.getMsg());
        return map;
    }

    @PostMapping("/deleteHistory")
    @ResponseBody
    public Object deleteHistory(@RequestBody AirbnblistingsVO airbnblistings){
        logger.info("registerUser{}" + JSONObject.toJSONString(airbnblistings));
        HashMap<String, Object> map = new HashMap<>();
        StopWatch watch = new StopWatch();
        watch.start();
        BaseResp<AirbnblistingsVO> userResp = airbnblistingsService.deleteHistory(airbnblistings);
        watch.stop();
        long totalTime = watch.getTotalTimeMillis();
        map.put("duration", totalTime);
        map.put("result", new ArrayList<>());
        map.put("message", userResp.getMsg());
        return map;
    }

    @PostMapping("/deleteFav")
    @ResponseBody
    public Object deleteFav(@RequestBody AirbnblistingsVO airbnblistings){
        logger.info("registerUser{}" + JSONObject.toJSONString(airbnblistings));
        HashMap<String, Object> map = new HashMap<>();
        StopWatch watch = new StopWatch();
        watch.start();
        BaseResp<AirbnblistingsVO> userResp = airbnblistingsService.deleteFavoByUserNameUrl(airbnblistings);
        watch.stop();
        long totalTime = watch.getTotalTimeMillis();
        map.put("duration", totalTime);
        map.put("result", new ArrayList<>());
        map.put("message", userResp.getMsg());
        return map;
    }
//    @GetMapping("/queryAllCountries")
//    @ResponseBody
//    public Object queryAllCountries() {
//        logger.info("QueryAllCountries{}");
//        HashMap<String, Object> map = new HashMap<>();
//        StopWatch watch = new StopWatch();
//        watch.start();
//        BaseResp countryList = airbnblistingsService.queryAllCountries();
//        map.put("countryList", countryList);
//        watch.stop();
//        long totalTime = watch.getTotalTimeMillis();
//        map.put("duration", totalTime);
//        return map;
//    }
//



    public class ResObjList {
        public List<ResObjListing> list = new ArrayList<>();
    }
}


