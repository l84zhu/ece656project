package com.searcher.rest.model;

import java.io.Serializable;

public class BaseResp<T> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3218759904061373504L;
    /**
     *
     */
    private String msg;

    private String resJson;

    private T body;


    public BaseResp() {

    }

    public BaseResp(String msg) {
        this.msg = msg;
    }

    public BaseResp(String msg, T body) {
        this.msg = msg;
        this.body = body;
    }

    public BaseResp(T body) {
        this.body = body;
    }

    /**
     * @return the body
     */
    public T getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    public void setBody(T body) {
        this.body = body;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getResJson() {
        return resJson;
    }

    public void setResJson(String resJson) {
        this.resJson = resJson;
    }

    @Override
    public String toString() {
        return "BaseResp{" +
                "msg='" + msg + '\'' +
                ", resJson='" + resJson + '\'' +
                ", body=" + body +
                '}';
    }
}