package com.searcher.rest.model;

import lombok.Data;

@Data
public class ResObjFav {
    public String name;
    public Integer price;
    public String streetName;
    public String urlListing;
    public String memo;
}
