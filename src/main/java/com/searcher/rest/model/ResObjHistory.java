package com.searcher.rest.model;

import lombok.Data;

import java.util.Date;

@Data
public class ResObjHistory {
    public String searchCondition;
    public Date searchDateTime;
}
