package com.searcher.rest.model;

import lombok.Data;

@Data
public class ResObjListing {
    public String name;
    public Integer price;
    public String streetName;
    public String urlListing;
}
