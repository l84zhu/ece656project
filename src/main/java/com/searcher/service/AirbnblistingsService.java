package com.searcher.service;
import com.searcher.rest.model.BaseResp;
import com.searcher.rest.model.AirbnblistingsVO;

public interface AirbnblistingsService {
    BaseResp<AirbnblistingsVO> queryListingsByConditionSelective(AirbnblistingsVO airbnblistings);

    BaseResp<AirbnblistingsVO> getHistoryByUserDay(AirbnblistingsVO airbnblistings);

    BaseResp<AirbnblistingsVO> addToFavorites(AirbnblistingsVO airbnblistings);

    BaseResp<AirbnblistingsVO> getFavoritesByUser(AirbnblistingsVO airbnblistings);

    BaseResp<AirbnblistingsVO> registerUser(AirbnblistingsVO airbnblistings);

    BaseResp<AirbnblistingsVO> deleteFavoByUserNameUrl(AirbnblistingsVO airbnblistings);

    BaseResp<AirbnblistingsVO> deleteHistory(AirbnblistingsVO airbnblistings);
}