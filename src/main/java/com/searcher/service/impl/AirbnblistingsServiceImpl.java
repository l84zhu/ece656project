package com.searcher.service.impl;
import com.alibaba.fastjson.JSON;
import com.searcher.dao.model.AirbnblistingsDO;
import com.searcher.rest.model.BaseResp;
import com.searcher.rest.model.AirbnblistingsVO;
import com.searcher.service.AirbnblistingsService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.text.SimpleDateFormat;
import java.util.List;



@Component
public class AirbnblistingsServiceImpl implements AirbnblistingsService {
    public String convert(String str) {
        char[] ch = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ',') ch[i] = '|';
            else if  (str.charAt(i) == ' ') i++;
            else ch[i] = str.charAt(i);
        }
        return new String(ch);
    }

    @Autowired
    private com.searcher.dao.dao.AirbnblistingsDOMapper AirbnblistingsDOMapper;

    @Override
    public BaseResp<AirbnblistingsVO> queryListingsByConditionSelective(AirbnblistingsVO airbnblistings) {
        BaseResp<AirbnblistingsVO> resp = new BaseResp<>();
        AirbnblistingsDO record = new AirbnblistingsDO();
        BeanUtils.copyProperties(airbnblistings, record);
        if(record.getUsername() == null){
            resp.setMsg("Please enter user name");
            return resp;
        }else{
            List<AirbnblistingsDO> user = AirbnblistingsDOMapper.getUserByName(record.getUsername());
            if(user.isEmpty()){
                resp.setMsg("Please enter correct user name");
                return resp;
            }else{
                record.setUserid(user.get(0).getUserid());
            }
        }
        if (record.getAmenities() != null){
            record.setAmenities(convert(record.getAmenities()));
        }
        List<AirbnblistingsDO> listingInfo = AirbnblistingsDOMapper.selectListingsByCond(record);
        if (listingInfo.isEmpty()) {
            resp.setMsg("No such rooms");
        } else {
            resp.setResJson(JSON.toJSONString(listingInfo));
            resp.setMsg("Success");
        }
        AirbnblistingsDO history = new AirbnblistingsDO();
        history.setUserid(record.getUserid());
        history.setSearchCondition(JSON.toJSONString(record));
        AirbnblistingsDOMapper.recordHistory(history);
        return resp;
    }

    @Override
    public BaseResp<AirbnblistingsVO> addToFavorites(AirbnblistingsVO airbnblistings) {
        BaseResp<AirbnblistingsVO> resp = new BaseResp<>();
        AirbnblistingsDO record = new AirbnblistingsDO();
        BeanUtils.copyProperties(airbnblistings, record);
        if(record.getUsername() == null){
            resp.setMsg("Please enter user name");
            return resp;
        }else{
            List<AirbnblistingsDO> user = AirbnblistingsDOMapper.getUserByName(record.getUsername());
            if(user.isEmpty()){
                resp.setMsg("Please enter correct user name");
                return resp;
            }else{
                record.setUserid(user.get(0).getUserid());
            }
        }
        List<AirbnblistingsDO> userInfo = AirbnblistingsDOMapper.checkFavo(record);
        if (!userInfo.isEmpty()) {
            resp.setMsg("Room already in favorites");
        } else {
            int res = AirbnblistingsDOMapper.add(record);
            if (res == 1) {
                resp.setMsg("Room has been added to favorites or user not exists");
            }else if(res == 0){
                resp.setMsg("Please enter correct listing url");
            }
        }
        return resp;
    }

    @Override
    public BaseResp<AirbnblistingsVO> getFavoritesByUser(AirbnblistingsVO airbnblistings) {
        BaseResp<AirbnblistingsVO> resp = new BaseResp<>();
        if(airbnblistings.getUsername() == null){
            resp.setMsg("Please enter the user name to search history");
            return resp;
        }else{
            List<AirbnblistingsDO> user = AirbnblistingsDOMapper.getUserByName(airbnblistings.getUsername());
            if(user.isEmpty()){
                resp.setMsg("Please enter correct user name");
                return resp;
            }else{
                airbnblistings.setUserid(user.get(0).getUserid());
            }
        }
        List<AirbnblistingsDO> favoRooms = AirbnblistingsDOMapper.selectFavoByUser(airbnblistings.getUserid());
        if (favoRooms.isEmpty()) {
            resp.setMsg("User " + JSON.toJSONString(airbnblistings.getUserid()) + "'s favorites is empty");
        } else {
            resp.setResJson(JSON.toJSONString(favoRooms));
            resp.setMsg("User " + JSON.toJSONString(airbnblistings.getUserid()) + "'s favorite room(s)");
        }
        return resp;
    }

    @Override
    public BaseResp<AirbnblistingsVO> getHistoryByUserDay(AirbnblistingsVO airbnblistings) {
        BaseResp<AirbnblistingsVO> resp = new BaseResp<>();
        AirbnblistingsDO record = new AirbnblistingsDO();
        BeanUtils.copyProperties(airbnblistings, record);
        if(record.getUsername() == null){
            resp.setMsg("Please enter the user name to search history");
            return resp;
        }else{
            List<AirbnblistingsDO> user = AirbnblistingsDOMapper.getUserByName(record.getUsername());
            if(user.isEmpty()){
                resp.setMsg("Please enter correct user name");
                return resp;
            }else{
                record.setUserid(user.get(0).getUserid());
            }
        }
        List<AirbnblistingsDO> history = AirbnblistingsDOMapper.selectHistrByUserDay(record);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(airbnblistings.getSearchDateTime());
        String date1 = formatter.format(airbnblistings.getSearchDateTimeEnd());
        if (history.isEmpty()) {
            resp.setMsg("No history available for user " + JSON.toJSONString(airbnblistings.getUsername()) + " between " + date + " and " + date1);
        } else {
            resp.setResJson(JSON.toJSONString(history));
            resp.setMsg("Search history for user " + JSON.toJSONString(airbnblistings.getUsername()) + " between " + date + " and " + date1 + ":");
        }
        return resp;
    }

    @Override
    public BaseResp<AirbnblistingsVO> registerUser(AirbnblistingsVO airbnblistings){
        BaseResp<AirbnblistingsVO> resp = new BaseResp<>();
        AirbnblistingsDO record = new AirbnblistingsDO();
        BeanUtils.copyProperties(airbnblistings, record);
        if(record.getUsername() == null){
            resp.setMsg("User name should not be null");
            return resp;
        }
        List<AirbnblistingsDO> checkUsers = AirbnblistingsDOMapper.getUserByName(record.getUsername());
        if (!checkUsers.isEmpty()) {
            resp.setMsg("User name exists, please change another name");
        } else {
            int res = AirbnblistingsDOMapper.addUser(record);
            if (res == 1) {
                resp.setMsg("Added user "+record.getUsername() + " !");
            }else if(res == 0){
                resp.setMsg("Not able to add user");
            }
        }
        return resp;
    }

    @Override
    public BaseResp<AirbnblistingsVO> deleteFavoByUserNameUrl(AirbnblistingsVO airbnblistings) {
        BaseResp<AirbnblistingsVO> resp = new BaseResp<>();
        AirbnblistingsDO record = new AirbnblistingsDO();
        BeanUtils.copyProperties(airbnblistings, record);
        if(record.getUsername() == null){
            resp.setMsg("User name should not be null");
            return resp;
        }else{
            List<AirbnblistingsDO> user = AirbnblistingsDOMapper.getUserByName(record.getUsername());
            if(user.isEmpty()){
                resp.setMsg("Please enter correct user name");
                return resp;
            }else{
                record.setUserid(user.get(0).getUserid());
            }
        }
        int res = AirbnblistingsDOMapper.deleteFavoByUserNameUrl(record);
        if (res == 1) {
            resp.setMsg("Favorite deleted");
        } else {
            resp.setMsg("No such room in your favorites");
        }
        return resp;
    }

    @Override
    public BaseResp<AirbnblistingsVO> deleteHistory(AirbnblistingsVO airbnblistings) {
        BaseResp<AirbnblistingsVO> resp = new BaseResp<>();
        AirbnblistingsDO record = new AirbnblistingsDO();
        BeanUtils.copyProperties(airbnblistings, record);
        if(record.getUsername() == null){
            resp.setMsg("User name should not be null");
            return resp;
        }else{
            List<AirbnblistingsDO> user = AirbnblistingsDOMapper.getUserByName(record.getUsername());
            if(user.isEmpty()){
                resp.setMsg("Please enter correct user name");
                return resp;
            }else{
                record.setUserid(user.get(0).getUserid());
            }
        }
        int res = AirbnblistingsDOMapper.deleteHistory(record);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(airbnblistings.getSearchDateTime());
        String date1 = formatter.format(airbnblistings.getSearchDateTimeEnd());
        if (res >= 1) {
            resp.setMsg("Search history for user " + JSON.toJSONString(airbnblistings.getUsername()) + " between " + date + " and " + date1 + " has been deleted");
        } else {
            resp.setMsg("No history available for user " + JSON.toJSONString(airbnblistings.getUsername()) + " between " + date + " and " + date1);
        }
        return resp;
    }
    
}


